import React from 'react';
import { StyleSheet, Text, View,TextInput, Button, Image } from 'react-native';

export default class App extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
          searchString: 'welcome'
        };
      }
    
      _onSearchTextChanged = (event) => {
        
        this.setState({ searchString: event.nativeEvent.text });
        //console.log('Current: '+this.state.searchString+', Next: '+event.nativeEvent.text);
    
      };
  _onSearchPressed = () => {
        const query = urlForQueryAndPage('place_name', this.state.searchString, 1);
        this._executeQuery(query);
      };
    
      testing1 = () => {
        console.log("testing1 reached");
    
      };
      returnapiurl = () => {
            const url = 'http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/';
            return url;
          }
        
          _executeQuery = (apiurl) => {
            console.log("reached _executeQuery and the query is " + apiurl);
        
            fetch(apiurl)
            .then((response) => response.json())
            .then(console.log(response))
            .catch(error =>
               {
                 console.log("something went wrong with fetch");
               });
        
          }
          XMLQuery = (apiurl) => {
            var request = new XMLHttpRequest();
            request.onreadystatechange = (e) => {
              if (request.readyState !== 4) {
                return;
              }
            
              if (request.status === 200) {
                console.log('success', request.responseText);
              } else {
                console.warn('error');
              }
            };
            
            request.open('GET', 'http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/');
            request.send();
          }
  render() {
    return (
      <View style={styles.container}>
       
        <Text style={{fontSize: 20}}>Ameesha.....!!!!!!</Text>
        <View style={styles.flowRight}>
        <TextInput style={{fontSize: 15}}
             style={styles.searchInput}
            value={this.state.searchString}
           onChange={this._onSearchTextChanged}
             placeholder='type anything'/>

             <Button
             onPress={() => {
               console.log("button pressed");
               console.log("entered value is "+this.state.searchString);
               this.testing1();
             }}
             color='#48BBEC'
        
            title='Hit me 1'

           />
        </View>
        <Image source={require('./Ameesha_Patel_at_Farah_Khan.jpg')} />
        <Text></Text>
        <Button
        onPress={() => {
          console.log("button 2 pressed");
          const apiurl = this.returnapiurl();
          console.log("URL for API is "+apiurl);
          //this._executeQuery(apiurl);
         this.XMLQuery(apiurl);
        }}
        color='#48BBEC'
        title='Hit me 2'
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flowRight: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',

  },
  searchInput: {
    width: 100,
    fontSize: 22,
  },
});
